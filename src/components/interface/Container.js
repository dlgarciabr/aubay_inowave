import React from "react";
import View from "./View";

const Container = (props) => {

    const sortedValues = props.values.sort((a, b) => a.value - b.value);

    const min = sortedValues[0];
    const max = sortedValues[sortedValues.length - 1];
    const avg = props.values
        .map(o => o.value)
        .reduce((accum, curr) => accum + curr) / props.values.length;

    return (
        <View
            name={props.interfaceId}
            min={min}
            max={max}
            avg={avg}
        />
    );
}

export default Container;